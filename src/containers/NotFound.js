import React from "react";
import Container from "@material-ui/core/Container";

export default () =>
    <Container>
        <h3>Página não encontrada</h3>
    </Container>;
