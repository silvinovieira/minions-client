import React, { Component } from "react";
import { API } from "aws-amplify";
import MinionsGrid from "../components/MinionsGrid";

export default class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            minions: []
        };
    }

    async componentDidMount() {
        try {
            const apiMinions = await API.get("minions", "/minions");
            this.setState({ minions: apiMinions });
        } catch (e) {
            alert(e);
        }
        this.setState({ isLoading: false });
    }

    static sendEmail(mailTo, minion) {
        API.post("minions", "/orders/email", {
            body: {
                email: mailTo,
                minionTitle: minion.title,
                minionDescription: minion.description,
            }
        }).then(
            r => alert(`Minion enviado para ${mailTo}`)
        ).catch(
            error => alert("Erro ao enviar e-mail")
        );
    }

    static renderMinions(minionsToRender) {
        return <MinionsGrid minions={minionsToRender} handleSendEmail={Home.sendEmail}/>;
    }

    render() {
        return (
            <div>
                {!this.state.isLoading && Home.renderMinions(this.state.minions)}
            </div>
        );
    }
}
