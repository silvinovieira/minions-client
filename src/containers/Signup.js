import React, { Component } from "react";
import { Auth } from "aws-amplify";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import SignupForm from "../components/SignupForm";
import ConfirmationForm from "../components/ConfirmationForm";

export default class Signup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            email: "",
            password: "",
            confirmPassword: "",
            confirmationCode: "",
            newUser: null
        };
    }

    validateForm() {
        return (
            this.state.email.length > 0 &&
            this.state.password.length > 0 &&
            this.state.password === this.state.confirmPassword
        );
    }

    validateConfirmationForm() {
        return this.state.confirmationCode.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleSubmit = async event => {
        event.preventDefault();
        if (this.validateForm()){
            this.setState({ isLoading: true });

            try {
                const newUser = await Auth.signUp({
                    username: this.state.email,
                    password: this.state.password
                });
                this.setState({
                    newUser
                });
            } catch (e) {
                alert(e.message);
            }

            this.setState({ isLoading: false });
        } else {
            alert("Dados inválidos");
        }
    };

    handleConfirmationSubmit = async event => {
        event.preventDefault();

        if(this.validateConfirmationForm()) {
            this.setState({ isLoading: true });

            try {
                await Auth.confirmSignUp(this.state.email, this.state.confirmationCode);
                await Auth.signIn(this.state.email, this.state.password);

                this.props.userHasAuthenticated(true);
                this.props.history.push("/");
            } catch (e) {
                alert(e.message);
                this.setState({ isLoading: false });
            }
        } else {
            alert("É necessário informar o código de validação");
        }
    };

    render() {
        return (
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                {this.state.newUser === null
                    ? <SignupForm
                        email={this.state.email}
                        password={this.state.password}
                        confirmPassword={this.state.confirmPassword}
                        isLoading={this.state.isLoading}
                        onChange={this.handleChange}
                        onSubmit={this.handleSubmit}
                      />
                    : <ConfirmationForm
                        confirmationCode={this.state.confirmationCode}
                        isLoading={this.state.isLoading}
                        onChange={this.handleChange}
                        onSubmit={this.handleConfirmationSubmit}
                    />
                }
            </Container>
        );
    }
}
