import React from 'react';
import { Link } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box'
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";

const useStyles = makeStyles({
    root: {
        flexGrow: 1,
    },
    title: {
        flexGrow: 1,
    },
    link: {
        color: 'white',
        textDecoration: 'none'
    }
});

export default function MinionsAppBar(props) {
    const classes = useStyles();

    return (
        <Box className={classes.root} paddingBottom={2}>
            <AppBar position="static">
                <Toolbar>
                    <Typography className={classes.title} variant="h6">
                        <Link to="/" className={classes.link}>Minions Store</Link>
                    </Typography>
                    {props.isAuth
                        ? <Button onClick={props.handleLogout} color="inherit" variant="outlined">Sair</Button>
                        : <ButtonGroup color="inherit" variant="outlined" size="small" component="appbar">
                            <Button href="/login">Entrar</Button>
                            <Button href="/signup">Cadastrar</Button>
                          </ButtonGroup>
                    }
                </Toolbar>
            </AppBar>
        </Box>
    );
}
