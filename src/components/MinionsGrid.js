import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import MinionCard from "./MinionCard";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    card: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

export default function MinionsGrid(props) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                {props.minions ? (
                    props.minions.map(
                        (minion, i) =>
                            <Grid item xs sm={4} md={3} key={i}>
                                <MinionCard
                                    className={classes.card}
                                    minion={minion}
                                    handleSendEmail={props.handleSendEmail}
                                />
                            </Grid>
                    )
                ) : (
                    <p>Não há minions disponíveis</p>
                )}
            </Grid>
        </div>
    );
}
