import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Card, CardActions, CardContent, CardMedia } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import FormDialog from "./FormDialog";

const useStyles = makeStyles({
    card: {
        minWidth: 150,
        maxWidth: 300,
    },
    media: {
        height: 150,
    },
});

export default function MinionCard(props) {
    const classes = useStyles();

    return (
        <Card className={classes.card}>
            <CardMedia
                className={classes.media}
                image={props.minion.img}
                title="Minion"
            />
            <CardContent>
                <Typography variant="h6" color="primary" gutterBottom>
                    {props.minion.title}
                </Typography>
                <Typography variant="body2" component="p">
                    {props.minion.description}
                </Typography>
            </CardContent>
            <CardActions>
                <FormDialog handleSend={props.handleSendEmail} minion={props.minion}/>
            </CardActions>
        </Card>
    );
}
