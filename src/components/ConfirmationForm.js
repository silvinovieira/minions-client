import React from 'react';
import Button from '@material-ui/core/Button';
import CircularProgress from "@material-ui/core/CircularProgress";
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import FormEmailField from "./FormEmailField";

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function ConfirmationForm(props) {
    const classes = useStyles();

    return (
        <div className={classes.paper}>
            <form onSubmit={props.onSubmit} className={classes.form} noValidate>
                <TextField
                    value={props.confirmationCode}
                    onChange={props.onChange}
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="confirmationCode"
                    label="Código de confirmação"
                    id="confirmationCode"
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    disabled={props.isLoading}
                    className={classes.submit}
                >
                    {props.isLoading ? <CircularProgress /> : "Confirmar"}
                </Button>
            </form>
        </div>
    );
}