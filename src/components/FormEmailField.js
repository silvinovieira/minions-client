import TextField from "@material-ui/core/TextField";
import React from "react";

export default function FormEmailField(props) {
    return (
        <TextField
            value={props.value}
            onChange={props.onChange}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="E-mail"
            name="email"
            autoComplete="email"
            autoFocus
        />
    );
}