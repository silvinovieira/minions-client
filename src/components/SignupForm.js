import React from 'react';
import Button from '@material-ui/core/Button';
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from '@material-ui/core/styles';
import FormEmailField from "./FormEmailField";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function SignupForm(props) {
    const classes = useStyles();

    return (
        <div className={classes.paper}>
            <form onSubmit={props.onSubmit} className={classes.form} noValidate>
                <FormEmailField value={props.email} onChange={props.onChange}/>
                <TextField
                    value={props.password}
                    onChange={props.onChange}
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Senha"
                    type="password"
                    id="password"
                />
                <TextField
                    value={props.confirmPassword}
                    onChange={props.onChange}
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="confirmPassword"
                    label="Confirmar senha"
                    type="password"
                    id="confirmPassword"
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    disabled={props.isLoading}
                    className={classes.submit}
                >
                    {props.isLoading ? <CircularProgress /> : "Cadastrar"}
                </Button>
            </form>
        </div>
    );
}