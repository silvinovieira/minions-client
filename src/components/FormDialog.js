import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormEmailField from "./FormEmailField";

export default function FormDialog(props) {
    const [open, setOpen] = React.useState(false);
    const [email, setEmail] = React.useState('');

    const handleChange = event => {
        setEmail(event.target.value);
    };

    function handleClickOpen() {
        setOpen(true);
    }

    function handleClickSend() {
        props.handleSend(email, props.minion);
        setEmail('');
        handleClose();
    }

    function handleClose() {
        setOpen(false);
    }

    return (
        <div>
            <Button variant="outlined" color="primary" size="small" onClick={handleClickOpen}>
                Reservar
            </Button>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Reserva</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Para reservar basta enviar seu e-mail
                    </DialogContentText>
                    <FormEmailField onChange={handleChange}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="secondary">
                        Cancelar
                    </Button>
                    <Button onClick={handleClickSend} color="primary" variant="outlined">
                        Enviar
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
