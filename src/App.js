import React, {Component} from 'react';
import { Auth } from "aws-amplify";
import {Container} from "@material-ui/core";
import Routes from "./Routes";
import MinionsAppBar from "./components/MinionsAppBar";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAuthenticated: false,
            isAuthenticating: true
        };
    }

    async componentDidMount() {
        try {
            await Auth.currentSession();
            this.userHasAuthenticated(true);
        }
        catch(e) {
            if (e !== 'No current user') {
                alert(e);
            }
        }

        this.setState({ isAuthenticating: false });
    }

    userHasAuthenticated = authenticated => {
        this.setState({ isAuthenticated: authenticated });
    };

    handleLogout = async event => {
        await Auth.signOut();
        this.userHasAuthenticated(false);
    };

    render() {
        const childProps = {
            isAuthenticated: this.state.isAuthenticated,
            userHasAuthenticated: this.userHasAuthenticated
        };

        return (
            <Container>
                {!this.state.isAuthenticating &&
                    <MinionsAppBar isAuth={this.state.isAuthenticated} handleLogout={this.handleLogout}/>
                }
                <Routes childProps={childProps}/>
            </Container>
        );
    };
}

export default App;
